package org.saltations.sheets;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Acts as a source of Maps representing the rows in each worksheet.
 */

public class SheetReader
{
    private static final Logger log = LoggerFactory.getLogger(SheetReader.class);

    /**
     * The google spreadsheet id
     */

    private String sheetId;

    /**
     * Google Spreadsheets service.
     */

    private Sheets service;

    /**
     * Primary constructor
     *
     * @param service Spreadsheet service. Non-null.
     * @param sheetId Spreadsheet Id.  Non-null.
     */

    public SheetReader(Sheets service, String sheetId)
    {
        checkNotNull(service, "Spreadsheet service was not provided");
        checkNotNull(sheetId, "Spreadsheet ID was not provided");

        this.service = service;
        this.sheetId = sheetId;
    }

    /**
     * Reads each worksheet in a spreadsheet into a list of maps with each list
     * indexed in the map by the worksheet name.
     * <h4>Example: Getting the contents of the 'Person' worksheet</h4>
     * <pre>{@code
     *      Map<String, List<Map<String, ?>>> rowMapsByTableName = new SpreadsheetReader(service, spreadsheetId).to();
     *      List<Map<String,?>> personRows = rowMapsByTableName.get("Person");
     * }
     * </pre>
     *
     * @return A map of a list of maps with each list indexed in the map by the worksheet name.
     */

    public Map<String, List<Map<String, ?>>> to()
    {
        Map<String, List<Map<String,?>>> rowmapsByWorksheetName = new HashMap<>();

        try {

            /*
             * Get the names of the worksheets
             */

            Spreadsheet spreadsheet = service.spreadsheets().get(sheetId).setIncludeGridData(false).execute();

            List<String> worksheetNames = spreadsheet.getSheets().stream().map(x -> x.getProperties().getTitle()).collect(Collectors.toList());

            /*
             * For each workSsheet extract a map of col names and values
             */

            for (String worksheetName : worksheetNames) {

                log.debug("Reading worksheet {}", worksheetName);

                String tableRange = worksheetName + "!A1:ZZ500";

                /*
                 * Get the values from this range all at once.
                 */

                ValueRange response = service.spreadsheets().values()
                        .get(sheetId, tableRange)
                        .execute();

                List<List<Object>> rows = response.getValues();

                log.debug("Worksheet {} has {} rows", worksheetName, rows.size());

                /*
                 * If we have something, load it
                 */

                if (rows != null )
                {
                    /*
                     * Get the column names
                     */

                    final List<String> columnNames = rows.get(0).stream()
                            .map(o -> (String) o.toString())
                            .collect(Collectors.toList());

                    log.debug("Worksheet {} has {} columns", worksheetName, columnNames.size());
                    log.debug("Worksheet {} has column names [{}]", worksheetName, Joiner.on(',').join(columnNames));

                    /*
                     * Map each row to a map.
                     */

                    try
                    {
                        if (!rows.isEmpty())
                        {
                            List<Map<String, ?>> rowMap = rows.subList(1, rows.size() + 1)
                                    .stream()
                                    .map(r -> toMap(columnNames, r))
                                    .collect(Collectors.toList());

                            rowmapsByWorksheetName.put(worksheetName, rowMap);
                        }
                    }
                    catch (Exception e)
                    {
                        log.warn("Exception was thrown while reading worksheet {}", worksheetName);
                        log.warn("Exception was ", e);
                    }
                }
            }
        }
        catch (IOException e)
        {
            throw new IllegalStateException(e);
        }

        return rowmapsByWorksheetName;
    }

    /**
     * Combines an ordered list of column names with an ordered list of objects
     *
     * @param columnNames Ordered ist of coumn names. Non-null.
     * @param row Ordered list of objects.  Non-null.
     *
     * @return A map of column name to value.
     */
    Map<String, Object> toMap(List<String> columnNames, List<Object> row)
    {
        checkNotNull(columnNames, "column names should be non-null.");
        checkNotNull(row, "row of values shopuld be non-null.");

        Map<String, Object> map = new HashMap<>();

        /*
         * For each column's value, add it with the column name to the map.
         */

        for (int i = 0; i < row.size(); i++)
        {
            String columnName = columnNames.get(i);
            Object value = row.get(i);

            map.put(columnName, value);
        }

        return map;
    }

}