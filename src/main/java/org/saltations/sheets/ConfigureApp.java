package org.saltations.sheets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class ConfigureApp
{
    private static final Logger log = LoggerFactory.getLogger(ConfigureApp.class);

    /**
     * Application for configuring the connection to the spreadsheet
     */

    public static void main(String...args)
    {
        // Prints the names and majors of students in a sample spreadsheet:
        // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit

        String spreadsheetId = "1XkwGfAfFH9bgUJ51hk_-lrVUMmG2CJI9URI28lp7QqU";

        SheetSource source = new SheetSource("jdbc-sheets", ConfigureApp.class.getResourceAsStream("/client_id.json"));

        Map<String, List<Map<String,?>>> rowMapsByTableName = source.sheet(spreadsheetId).to();

        rowMapsByTableName.keySet().forEach(x -> System.out.println(x));
    }

}
