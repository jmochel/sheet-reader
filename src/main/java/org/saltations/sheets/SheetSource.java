package org.saltations.sheets;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static com.google.api.client.util.Preconditions.checkArgument;
import static com.google.api.client.util.Preconditions.checkNotNull;
import static java.lang.System.getProperty;

/**
 * A self contained source of access to the Google Spreadsheet
 */

public class SheetSource
{
    private static final Logger log = LoggerFactory.getLogger(SheetSource.class);

    /**
     * Application name.
     */

    private String applicationName;

    /**
     * Input stream for the credentials.
     */

    private InputStream credentialsStream;

    /**
     * Global instance of the {@link FileDataStoreFactory}.
     */

    private FileDataStoreFactory dataStoreFactory;

    /**
     * Global instance of the JSON factory.
     */

    private static final JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();

    /**
     * Global instance of the HTTP transport.
     */

    private static HttpTransport httpTransport;

    /**
     * Global instance of the scopes required by this quickstart.
     *
     * If modifying these scopes, delete your previously saved credentials
     * at ~/.credentials/jdbc-sheets
     */

    private static final List<String> SCOPES = Arrays.asList(SheetsScopes.SPREADSHEETS_READONLY);

    static
    {
        try
        {
            httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        }
        catch (Throwable e)
        {
            log.error("There was an error", e);
            System.exit(1);
        }
    }

    /**
     * Primary constructor
     *
     * @param applicationName Name of the Google API application. Non-null. Non-empty.
     * @param credentialsStream Input stream containing for the credentials used for authorization. Non-null.
     */

    public SheetSource(String applicationName, InputStream credentialsStream)
    {
        checkNotNull(applicationName, "We are missing the name of the application");
        checkArgument(!applicationName.isEmpty(), "We are missing the name of the application");
        checkNotNull(applicationName, "We are missing the credentials input stream");

        this.applicationName = applicationName;
        this.credentialsStream = credentialsStream;

        Path dataStoreFolder = Paths.get(getProperty("user.home"), ".credentials", applicationName);

        try
        {
            log.debug("Credentials will be saved to {}", dataStoreFolder.toAbsolutePath());
            this.dataStoreFactory = new FileDataStoreFactory(dataStoreFolder.toFile());
        }
        catch (IOException e)
        {
            throw new RuntimeException("Unable to icreate the datastore for autnetication",e);
        }
    }

    /**
     * Creates an authorized Credential object.
     *
     * @return an authorized Credential object.
     * @throws IOException
     */

    private Credential authorize() throws IOException
    {
        // In order to authorize access to the spreadsheet service we need

        // The client secrets.

        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(jsonFactory, new InputStreamReader(this.credentialsStream));

        // Build flow and trigger user authorization request.

        GoogleAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow.Builder(
                        httpTransport, jsonFactory, clientSecrets, SCOPES)
                        .setDataStoreFactory(this.dataStoreFactory)
                        .setAccessType("offline")
                        .build();

        // Recover credentials

        Credential credential = new AuthorizationCodeInstalledApp(
                flow, new LocalServerReceiver()).authorize("user");


        return credential;
    }

    /**
     * Build and return an authorized Sheets API client service.
     *
     * @return an authorized Sheets API client service
     *
     * @throws IOException
     */

    private Sheets getSheetsService() throws IOException
    {
        Credential credential = authorize();

        return new Sheets.Builder(httpTransport, jsonFactory, credential)
                .setApplicationName(this.applicationName)
                .build();
    }

    /**
     *
     * @param sheetId
     * @return
     */

    public SheetReader sheet(String sheetId)
    {
        // Build a new authorized API client service.

        Sheets service;

        try
        {
            service = getSheetsService();
        }
        catch (IOException e)
        {
            throw new IllegalStateException(e);
        }

        return new SheetReader(service, sheetId);
    }
}